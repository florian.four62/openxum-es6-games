"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Zertz from '../../../openxum-core/games/zertz/index.mjs';

class Manager extends OpenXum.Manager {
  constructor(t, e, g, o, s, w, f) {
    super(t, e, g, o, s, w, f);
    this.that(this);
  }

  build_move() {
    return new Zertz.Move();
  }

  get_current_color() {
    return this.engine().current_color() === Zertz.Color.ONE ? 'One' : 'Two';
  }

  static get_name() {
    return 'zertz';
  }

  get_winner_color() {
    return this.engine().winner_is() === Zertz.Color.ONE ? 'one' : 'two';
  }

  process_move() {
  }
}

export default Manager;